# BOIDS 2D Version 1.0
# Andrew MacRae (macrae [cat without the 'c'] uvic dot ca
import sys,pygame as pg
import numpy as np

# ------------------- Classes -------------------
class V3:
    """ Basic class to represent a real-values, 3-vector.
    Vector consists of 3 floats, rather than a numpy array by design choice"""
    def __init__(self,x0,y0,z0):
        self.x = x0
        self.y = y0
        self.z = z0

    def mag(self):
        return np.sqrt(self.x**2 + self.y**2 + self.z**2)

    def cross(self,b):
        '''Cross product'''
        return np.array([self.x,self.y,self.z]).cross(b)

    def scale(self,d:float):
        return V3(self.x*d, self.y*d, self.z*d)

    def add(self, b):
        return V3(self.x+b.x, self.y+b.y, self.z+b.z)

    def subt(self, b):
        return V3(self.x-b.x, self.y-b.y, self.z-b.z)

    def dot(self, b):
        return self.x*b.x + self.y*b.y + self.z*b.z

    def unit(self):
        if(self.mag()==0):
            return V3(0,0,0)
        else:
            return self.scale(1.0/self.mag())

class Boid2D:
    """ 2D Boid. Holds position and velocity 3-vectors. No drawing is specified"""
    def __init__(self,x0,y0,vx0,vy0):
        self.pos = V3(x0,y0,0)
        self.vel = V3(vx0, vy0, 0)

    def move(self, dt:float):
        '''Essentially, uses Euler integration'''
        self.pos = self.pos.add(self.vel.scale(dt))
        return

    def capvel(self, vmax:float):
        '''Clips the velocity to some max value'''
        mg = self.vel.mag()
        if mg > vmax:
            self.vel = self.vel.scale(vmax/mg)
        return


# --------------------- Main Program ---------------------

# For drawing
W = 900
H = 600
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

srad = 50

# List of boids
berds = []
nBerds = 30

rules = {'wrapspace':True, 'centralforce':False, 'flock':True, 'centroid':True, 'collision':True, 'velfeedback':True}

#Initialize the Boid Array
for k in range(nBerds):
    velmax = 70

    berds.append(Boid2D(W/4+W*np.random.rand()/2, H/4+H*np.random.rand()/2, velmax*(np.random.rand()-.5), velmax*(np.random.rand()-.5)))

# Initialize PyGame Environment and screen
pg.init()
screen = pg.display.set_mode((W,H))
clock = pg.time.Clock()
pg.display.set_caption('PyBoids2D!!!')


# ---- Boid Movement Rules ----

# Rule 1: Follow the center of mass of the flock
def flock2centroid(idx,gain,searchrad = 1e8):
    centroid = V3(0,0,0)
    nsight = 0
    for k in range(nBerds):
        if k != idx and berds[k].pos.subt(berds[idx].pos).mag() < searchrad:
            centroid = centroid.add(berds[k].pos)
            nsight += 1
    if nsight > 0:
        centroid = centroid.scale(1.0 / float(nsight))
        return centroid.subt(berds[idx].pos).scale(gain)
    else:
        return V3(0,0,0)

# Rule 2: Don't bang into each other
def avoidcollsions(idx,gain):
    birdRepellant = V3(0,0,0)
    temp = V3(0, 0, 0)
    maxdist = 50

    for k in range(nBerds):
        if k != idx:
            temp = berds[k].pos.subt(berds[idx].pos)
            if temp.mag() < maxdist:
                den = max(temp.mag(),1.0)
                birdRepellant = birdRepellant.add(temp.scale(-gain/den**2))
    return birdRepellant

# Rule 3: Try to match velocity of nearby boids (velocity follows centroid of velocity)
def flocktogether(idx,gain,searchrad=1e8):
    centroid = V3(0,0,0)
    nsight = 0
    for k in range(nBerds):
        if k!=idx and berds[k].pos.subt(berds[idx].pos).mag() < searchrad:
            centroid = centroid.add(berds[k].vel)
            nsight += 1
    if nsight > 0:
        centroid = centroid.scale(1.0 / float(nsight))
        return centroid.subt(berds[idx].vel).scale(gain)
    else:
        return V3(0,0,0)

# Rule 4: Try to fly at cruise velocity
def velfeedback(idx,setpoint,gain):
    return berds[k].vel.unit().scale(gain*(setpoint - berds[k].vel.mag()))

# Rule 5: stay near the central location
def centralize(idx,x0,y0,z0,gain):
    center = V3(x0,y0,z0)
    return berds[idx].pos.subt(center).scale(-gain)


# ------ Main Program Loop --------
while(True):
    clock.tick(33)

# Handle events (check for exit)
    for event in pg.event.get():
        if event.type == pg.QUIT:
            sys.exit()

# Think boids ... think
    for k in range(nBerds):
        acc = V3(0,0,0)
    # Apply rules
        if rules['centroid']:
            acc = acc.add(flock2centroid(k, 5.5e-2,searchrad = srad))
        if rules['collision']:
            acc = acc.add(avoidcollsions(k, 2.5e1))
        if rules['flock']:
            acc = acc.add(flocktogether(k, 2.5e-1,searchrad = srad))
        if rules['velfeedback']:
            acc = acc.add(velfeedback(k,30,5e-2))
        if(rules['centralforce']):
            acc = acc.add(centralize(k,W/2,H/2,0, 5e-3))

        berds[k].vel = berds[k].vel.add(acc)
        berds[k].capvel(42.2)

# Move 'dem boids
    for k in range(nBerds):
        berds[k].move(.15)
    # Wrap space (toroidal mapping)
        if rules['wrapspace']:
            if berds[k].pos.x > W:
                berds[k].pos.x -= W
            if berds[k].pos.x < 0:
                berds[k].pos.x += W
            if berds[k].pos.y > H:
                berds[k].pos.y -= H
            if berds[k].pos.y < 0:
                berds[k].pos.y += H

# Do all drawing here ...
    screen.fill(BLACK)
    for k in range(nBerds):
        pg.draw.circle(screen,WHITE,(int(berds[k].pos.x),int(berds[k].pos.y)),5)
        pg.draw.line(screen,WHITE,(int(berds[k].pos.x),int(berds[k].pos.y)),(int(berds[k].pos.add(berds[k].vel.unit().scale(15)).x),int(berds[k].pos.add(berds[k].vel.unit().scale(10)).y)),3)
    pg.display.flip()